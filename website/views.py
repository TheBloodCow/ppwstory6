from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import OnePost
from .forms import Add_Post

# Remember: EVERYTHING IN VIEWS MUST BE SPECIFIED IN URLS.PY OF THE PROJECT
def profile(request):
	project = OnePost.objects.all()
	response['project']=project
	return render(request, "index.html", response)
def addPost(request):
	response['addPost_form'] = Add_Post()
	return render(request, "addPost.html", response)

def deleteAll(request):
	project = OnePost.objects.all().delete()
	return HttpResponseRedirect(reverse('profile'))

def saveproject(request):
	form = Add_Post(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['Status'] = request.POST['Status']
		project = OnePost(Status=response['Status'])
		project.save()
		print("sukses")
		return HttpResponseRedirect(reverse('profile'))
	else:
		return HttpResponseRedirect('/')

response = {}
