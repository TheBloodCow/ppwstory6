from django.urls import path
from django.conf.urls import url
from .views import profile, addPost, deleteAll

#url for app
urlpatterns = [
    path('', profile, name ='profile'),
    path('addPost', addPost, name = 'addPost'),
    path('deleteAll', deleteAll, name = 'deleteAll'),  
]
